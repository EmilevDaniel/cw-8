import React from 'react';
import {useState} from "react";
import axiosApi from "../../axiosApi";
import './AddForm.css'


const AddForm = ({history}) => {
    const [quote, setQuotes] = useState({
        author: '',
        text: '',
        category: '',
    });

    const onInputChange = e => {
        const {name, value} = e.target;
        setQuotes(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const createPost = async e => {
        e.preventDefault();
        try {
            await axiosApi.post('/quotes.json', quote);
        } finally {
            history.replace('/');
        }
    };

    return (
        <div className="PostForm">
            <form onSubmit={createPost}>
                <select name="category"
                        id="category"
                        onChange={onInputChange}
                        value={quote.category}
                >
                    <option value=""></option>
                    <option value="starWars">Star Wars</option>
                    <option value="famousPeople">Famous People</option>
                    <option value="saying">Saying</option>
                    <option value="humor">Humour</option>
                    <option value="motivational">Motivational</option>
                </select>
                <input className="Input"
                       name="author"
                       placeholder="Author"
                       value={quote.author}
                       onChange={onInputChange}/>
                <textarea className="Input"
                          name="text"
                          placeholder="Write something"
                          value={quote.text}
                          onChange={onInputChange}/>
                <button type="submit">SAVE</button>
            </form>
        </div>
    );
};

export default AddForm;