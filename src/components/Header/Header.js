import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css'

const Header = () => {
    return (
        <div className="Header">
            <h1>Quotes Central</h1>
            <div>
                <NavLink className='HeaderLink' to='/'>Home</NavLink>
                <NavLink className='HeaderLink' to='/add'>Add new quote</NavLink>
            </div>
        </div>
    );
};

export default Header;