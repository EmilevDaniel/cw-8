import React from 'react';
import {NavLink} from "react-router-dom";

const SideBar = () => {
    return (
        <div>
            <NavLink className='HeaderLink' to='/'>All</NavLink>
            <NavLink className='HeaderLink' to='/quotes/starWars'>Star wars</NavLink>
            <NavLink className='HeaderLink' to='/quotes/famousPeople'>Famous people</NavLink>
            <NavLink className='HeaderLink' to='/quotes/saying'>Saying</NavLink>
            <NavLink className='HeaderLink' to='/quotes/humor'>Humor</NavLink>
            <NavLink className='HeaderLink' to='/quotes/motivational'>Motivational</NavLink>
        </div>
    );
};

export default SideBar;