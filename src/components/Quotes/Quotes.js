import React from 'react';
import {useState, useEffect} from "react";
import axiosApi from "../../axiosApi";
import Quote from "../Quote/Quote";

const Quotes = () => {
    const [newPosts, setNewPosts] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/quotes.json');
            setNewPosts(Object.entries(response.data));
        };
        fetchData().catch(e => console.error(e));
    }, []);

    return (
        <div>
            <h1>ALL</h1>
            <div className='PostsHolder'>
                {newPosts.map((data, index) => {
                    return (<Quote key={index} quote={data}/>);
                })}
            </div>
        </div>
    );
};

export default Quotes;