import React from 'react';
import {useState, useEffect} from "react";
import axiosApi from "../../axiosApi";
import Header from "../Header/Header";


const EditForm = ({history, match}) => {
    const [EditQuotes, setEditQuotes] = useState({
        author: '',
        text: '',
        category: '',
    });

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get(`quotes/${match.params.id}.json`);
            setEditQuotes(prev => ({
                ...prev,
                author: response.data.author,
                text: response.data.text,
                category: response.data.category,
            }))
        };

        fetchData().catch(console.error)
    }, [match.params.id]);

    const onInputChange1 = e => {
        const {name, value} = e.target;

        setEditQuotes(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const EditPost = async e => {
        e.preventDefault();
        try {
            await axiosApi.put(`quotes/${match.params.id}.json`, EditQuotes);
        } finally {
            history.replace('/');
        }
    };

    return (
        <>
            <Header/>
            <div className="PostForm">
                <form onSubmit={EditPost}>
                    <select name="category"
                            id="category"
                            onChange={onInputChange1}
                            value={EditQuotes.category}
                    >
                        <option value=""></option>
                        <option value="starWars">Star Wars</option>
                        <option value="famousPeople">Famous People</option>
                        <option value="saying">Saying</option>
                        <option value="humor">Humour</option>
                        <option value="motivational">Motivational</option>
                    </select>
                    <input className="Input"
                           name="author"
                           placeholder="Author"
                           value={EditQuotes.author}
                           onChange={onInputChange1}/>
                    <textarea className="Input"
                              name="text"
                              placeholder="Write something"
                              value={EditQuotes.text}
                              onChange={onInputChange1}/>
                    <button type="submit">EDIT</button>
                </form>
            </div>
        </>
    );
};

export default EditForm;