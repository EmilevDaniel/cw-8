import React from 'react';
import './Quote.css'
import axiosApi from "../../axiosApi";
import {NavLink} from "react-router-dom";

const Quote = ({quote}) => {
    const deleteQuote = e => {
        const fetchData = async () => {
            axiosApi.delete(`/quotes/${quote[0]}/.json`);
        };
        fetchData().catch(e => console.error(e));
    };

    return (
        <div className='Post'>
            <div className='ButtonsHolder'>
                <button className='PostButton' onClick={deleteQuote}>X</button>
                <NavLink className='PostButton' to={'/edit/' + quote[0]}>EDIT</NavLink>
            </div>
            <p style={{color: '#ccc'}}>{quote[1].category}</p>
            <p>{quote[1].text}</p>
            <p>{quote[1].author}</p>
        </div>
    );
};

export default Quote;