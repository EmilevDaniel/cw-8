import React from 'react';
import {useState, useEffect} from "react";
import axiosApi from "../../axiosApi";
import Header from "../../components/Header/Header";
import SideBar from "../../components/SideBar/SideBar";
import Quote from "../../components/Quote/Quote";

const SelectedQuotes = ({match}) => {
    const [singlePost, setSinglePost] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get(`quotes.json?orderBy="category"&equalTo="${match.params.category}"`);
            setSinglePost(Object.entries(response.data));
        };

        fetchData().catch(console.error)
    }, [match.params.category]);

    return (
        <>
            <Header/>
            <SideBar/>
            <h1>{match.params.category ? match.params.category.toUpperCase() : null}</h1>
            {singlePost.map((data, index) => {
                return (<Quote key={index} quote={data}/>);
            })}
        </>
    )
};

export default SelectedQuotes;