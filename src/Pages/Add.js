import React from 'react';
import Header from "../components/Header/Header";
import AddForm from "../components/AddForm/AddForm";

const Add = ({history}) => {
    return (
        <div>
            <Header/>
            <AddForm history={history}/>
        </div>
    );
};

export default Add;