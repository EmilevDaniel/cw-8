import React from 'react';
import Header from "../components/Header/Header";
import Quotes from "../components/Quotes/Quotes";
import SideBar from "../components/SideBar/SideBar";

const MainPage = ({match}) => {
    return (
        <div>
            <Header/>
            <div>
                <SideBar/>
                <Quotes match={match}/>
            </div>
        </div>
    );
};

export default MainPage;