import React from 'react';
import MainPage from "./Pages/MainPage";
import './App.css';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Add from "./Pages/Add";
import SelectedQuotes from "./Pages/SelectedPost/SelectedQuotes";
import EditForm from "./components/EditForm/EditForm";

function App() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/" component={MainPage}/>
                    <Route path="/add" component={Add}/>
                    <Route path="/quotes/:category" component={SelectedQuotes}/>
                    <Route path="/edit/:id" component={EditForm}/>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
