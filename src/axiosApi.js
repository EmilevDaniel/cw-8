import axios from "axios";

const axiosApi = axios.create({
    baseURL: "https://emilev-cw8-default-rtdb.firebaseio.com/",
});

export default axiosApi;